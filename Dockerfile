FROM node

MAINTAINER Genar <gtrias@gmail.com>

COPY . /app

WORKDIR /app

ARG REACT_APP_API_URL=http://localhost:4000
ENV REACT_APP_API_URL=$REACT_APP_API_URL

RUN npm install && npm run build && npm run build-server

ENV NODE_ENV production

CMD npm start
