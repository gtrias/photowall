module.exports = {
  excludes: [
    './build/**'
  ],

  importStatementFormatter({ importStatement }) {
    return importStatement.replace(/;$/, '');
  },
}
