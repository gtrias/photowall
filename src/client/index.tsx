import 'babel-polyfill'
import 'classlist-polyfill'
import 'core-js/es6/map'
import 'core-js/es6/set'

import { ConnectedRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import * as React from 'react';
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Route } from 'react-router-dom'

import configureStore from './configureStore'
import WallContainer from './containers/WallContainer'
import registerServiceWorker from './registerServiceWorker'

import './styles/index.scss';

const history = createBrowserHistory()
const store = configureStore(history)

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <React.Fragment>
        <Route
          exact path="/"
          component={WallContainer}
        />
        <Route
          path="/picture/:id"
          component={WallContainer}
        />
      </React.Fragment>
    </ConnectedRouter>
  </Provider>

  ,document.getElementById('root') as HTMLElement
);
registerServiceWorker();
