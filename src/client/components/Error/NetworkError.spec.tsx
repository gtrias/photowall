import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import * as React from 'react'
import NetworkError from './NetworkError'

Enzyme.configure({ adapter: new Adapter() });


function setup(testProps = {}) {
  const props = {}

  const finalProps = {...props, ...testProps}

  const enzymeWrapper = Enzyme.shallow(
    <NetworkError {...finalProps} />
  )

  return {
    enzymeWrapper,
    props,
  }
}

describe('components', () => {
  describe('NetworkError', () => {
    it('should render NetworkError', () => {
      const { enzymeWrapper } = setup()

      expect(enzymeWrapper.find('.Error').exists()).toBe(true)
    })
  })
})
