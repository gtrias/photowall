import * as React from 'react'
import { IPicturesState } from '../../store/pictures/types'

export interface IProps {
  error: IPicturesState["error"]
}

class NetworkError extends React.Component<IProps> {
  public render() {
    return (
      <div className="Error">
        Oops, there was a problem with the API. { this.props.error }
      </div>
    );
  }
}

export default NetworkError
