import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import * as React from 'react'
import Picture from '../../../domain/Entity/Picture'
import Spinner from './Spinner'

Enzyme.configure({ adapter: new Adapter() });

function setup(testProps = {}) {
  const props = {
    pictures: {
      current: new Picture({author: {username: 'test'}})
    }
  }

  const finalProps = {...props, ...testProps}

  const enzymeWrapper = Enzyme.shallow(
    <Spinner {...finalProps} />
  )

  return {
    enzymeWrapper,
    props,
  }
}

describe('components', () => {
  describe('Spinner', () => {
    it('should render Spinner', () => {
      const picture = new Picture({url: 'http://testimg.jpg'})
      const { enzymeWrapper } = setup({picture})

      expect(enzymeWrapper.find('.spinner').exists()).toBe(true)
    })
  })
})
