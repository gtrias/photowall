import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import * as React from 'react'
import Picture from '../../../domain/Entity/Picture'
import Wall from './Wall';

Enzyme.configure({ adapter: new Adapter() });

function setup(testProps = {}) {
  const props = {
    dispatch: jest.fn,
    pictures: {
      error: null,
      keys: [],
      page: '1',
      pictures: [new Picture({id: '123', url: '1234'})],
    },
    routing: {
      history: {
        listen: () => {
          return null
        }
      }
    },
  }

  const finalProps = {...props, ...testProps}

  const enzymeWrapper = Enzyme.shallow(
    <Wall {...finalProps} />
  )

  return {
    enzymeWrapper,
    props,
  }
}


describe('components', () => {
  describe('Wall', () => {
    it('should render Wall without crashing', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find('.wall').exists()).toBe(true)
    })
  })
})
