import * as React from 'react'
import InfiniteScroll from 'react-infinite-scroller'
import Masonry from 'react-masonry-css'
import { Dispatch } from 'redux';

import {
  FETCH_PICTURES,
  PICTURES_NEXT_PAGE,
} from '../../constants/actionTypes'

import Picture from '../../../domain/Entity/Picture'
import { IPicturesState } from '../../store/pictures/types'
import NetworkError from '../Error/NetworkError'
import PictureThumbnail from '../Picture/PictureThumbnail'
import PictureViewer from '../PictureViewer/PictureViewer'

export interface IProps {
  dispatch: Dispatch
  pictures: IPicturesState
  routing: any
}

export default class Wall extends React.Component<IProps> {
  public constructor(props) {
    super(props)
    this.loadMore = this.loadMore.bind(this)
    this.showLoader = this.showLoader.bind(this)
    this.showLightbox = this.showLightbox.bind(this)
  }

  public componentWillMount() {
    if (!Object.keys(this.props.pictures.pictures).length) {
      this.props.dispatch({type: FETCH_PICTURES})
    }
  }

  public render() {
    const pictures = this.props.pictures.keys.map((key) => {
      const pic = new Picture(this.props.pictures.pictures[key])
      return (
        <PictureThumbnail
          key={pic.id}
          picture={pic}
          dispatch={this.props.dispatch}
        />
      )
    })

    return (
      <React.Fragment>
        { this.showLightbox() }
        <div
          className="wall"
        >
          { this.showErrors() }
          { this.showLoader() }

          <InfiniteScroll
            pageStart={0}
            loadMore={this.loadMore}
            initialLoad={false}
            hasMore={true}
          >
            <Masonry
              breakpointCols={{default: 5, 576: 1, 768: 2, 900: 3, 1024: 4}}
              className="picture-grid"
              columnClassName="picture-grid-column"
            >
              { pictures }
            </Masonry>
          </InfiniteScroll>
        </div>
      </React.Fragment>
    )
  }

  private showErrors() {
    let errorComp
    if (this.props.pictures.error) {
      errorComp =
        <NetworkError
          error={this.props.pictures.error}
        />
    }

    return errorComp
  }

  private loadMore(page) {
    if (!this.props.pictures.loading) {
      this.props.dispatch({type: PICTURES_NEXT_PAGE})
    }
  }

  private showLoader() {
    let loader
    if (this.props.pictures.loading) {
      loader = <div key='loader' className="loader">Loading ...</div>
    }

    return loader

  }

  private showLightbox() {
    let lightBox
    if (this.props.pictures.current) {
      lightBox = (
        <PictureViewer
          pictures={this.props.pictures}
          dispatch={this.props.dispatch}
        />
      )

      document.body.classList.add('overflow-hidden')
    } else {
      document.body.classList.remove('overflow-hidden')
    }

    return lightBox
  }
}
