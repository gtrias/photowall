import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import * as React from 'react'
import PictureThumbnail from './PictureThumbnail'
import Picture from '../../../domain/Entity/Picture'

Enzyme.configure({ adapter: new Adapter() });


function setup(testProps = {}) {
  const props = {}

  const finalProps = {...props, ...testProps}

  const enzymeWrapper = Enzyme.shallow(
    <PictureThumbnail {...finalProps} />
  )

  return {
    enzymeWrapper,
    props,
  }
}

describe('components', () => {
  describe('PictureThumbnail', () => {
    it('should render PictureThumbnail', () => {
      const picture = new Picture({url: 'http://testimg.jpg'})
      const { enzymeWrapper } = setup({picture})

      expect(enzymeWrapper.find('.picture').exists()).toBe(true)
    })
  })
})
