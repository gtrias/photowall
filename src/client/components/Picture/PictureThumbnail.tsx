import { push } from 'connected-react-router'
import * as React from 'react'
import { Dispatch } from 'redux';

import Picture from '../../../domain/Entity/Picture'
import LazyImage from '../Image/LazyImage'

export interface IProps {
  dispatch: Dispatch
  picture: Picture
}

export default class PictureThumbnail extends React.Component<IProps> {
  public state = {
    isHovered: false
  }

  constructor(props) {
    super(props)
    this.handleHover = this.handleHover.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  public render() {
    let overlay
    if (this.state.isHovered) {
      overlay = (
        <div className="picture-overlay">
          <ul className="list-unstyled">
            <li><h3>{ this.props.picture.title }</h3></li>
            <li>Author: { this.props.picture.author ? this.props.picture.author.username : 'not available' }</li>
          </ul>
        </div>
      )
    }

    return (
      <div
        className="picture"
        onMouseEnter={this.handleHover}
        onMouseLeave={this.handleHover}
        onClick={this.handleClick}
        style={{minHeight: 'auto'}}
      >
        <LazyImage
          src={this.props.picture.thumbUrl}
          srcAlt={this.props.picture.placeholderUrl}
        >
          <img
            src={this.props.picture.thumbUrl}
            alt={this.props.picture.placeholderUrl}
          />
        </LazyImage>

        { overlay }
      </div>
    )
  }

  private handleHover() {
    this.setState({
      isHovered: !this.state.isHovered
    });
  }

  private handleClick() {
    this.props.dispatch(push(`/picture/${ this.props.picture.id }`))
  }
}
