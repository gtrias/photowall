import * as React from 'react'
import Spinner from '../Spinner/Spinner'

export interface IProps {
  src: string
  placeholder?: string
  srcAlt?: string
}

class LazyImage extends React.Component<IProps> {
  public state = {
    error: null,
    loading: true,
  }

  // The Image object to check the loading will be stored here
  private image

  constructor(props) {
    super(props)

    this.handleLoad = this.handleLoad.bind(this)
    this.handleError = this.handleError.bind(this)
  }

  // Update component only if the src has changed
  public shouldComponentUpdate(nextProps) {
    if (nextProps.src !== this.props.src) {
      return false
    }

    return true
  }

  public componentWillReceiveProps(nextProps) {
    if (nextProps.src !== this.props.src) {
      this.destroyLoading()
      this.initializeLoading(nextProps.src)
    }
  }

  public componentWillMount() {
    this.initializeLoading(this.props.src)
  }

  public componentWillUnmount() {
    this.destroyLoading()
  }

  public render() {
    if (this.state.loading && !this.state.error) {
      return (
        <Spinner />
      )
    }

    return (
      <div className="lazy-image">
        { this.props.children }
      </div>
    );
  }

  // Initialize the loading parameters
  private initializeLoading(src) {
    this.image = new Image()

    this.image.src = src
    this.image.onload = this.handleLoad
    this.image.onerror = this.handleError

    this.setState({
      loading: true,
    })
  }

  // Reset the loading parameters
  private destroyLoading() {
    this.image = null

    this.setState({
      loading: false,
    })
  }

  private handleLoad(e) {
    this.setState({
      loading: false,
    })
  }

  private handleError(e) {
    this.setState({
      error: `Failed to load ${this.props.src}`,
      loading: false
    })
  }
}

export default LazyImage
