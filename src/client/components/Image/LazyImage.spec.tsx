import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import * as React from 'react'
import LazyImage from './LazyImage'

Enzyme.configure({ adapter: new Adapter() });


function setup(testProps = {}) {
  const props = {
    placeholder: 'test'
  }

  const finalProps = {...props, ...testProps}

  const enzymeWrapper = Enzyme.shallow(
    <LazyImage {...finalProps} />
  )

  return {
    enzymeWrapper,
    props,
  }
}

describe('components', () => {
  describe('LazyImage', () => {
    it('should render LazyImage', () => {
      const { enzymeWrapper } = setup()

      expect(enzymeWrapper.find('Spinner').exists()).toBe(true)
    })
  })
})
