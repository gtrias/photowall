import * as React from 'react'

import Picture from '../../../domain/Entity/Picture';

export interface IProps {
  picture: Picture
}

export default class Meta extends React.Component<IProps> {
  public render() {
    return (
      <div className="picture-meta">
        <ul className="list-unstyled">
          <li>Title: <a target="_blank" href={this.props.picture.postUrl}>{this.props.picture.title}</a></li>
          <li>Description: {this.props.picture.description}</li>
          <li>Author: {this.props.picture.author.username}</li>
          <li>Tags: {this.props.picture.tags.join(', ')}</li>
        </ul>
      </div>
    )
  }
}
