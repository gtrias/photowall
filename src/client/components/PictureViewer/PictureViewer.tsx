import { push } from 'connected-react-router'
import * as React from 'react'
import KeyHandler, {KEYPRESS} from 'react-key-handler'
import { Dispatch } from 'redux'

import Picture from '../../../domain/Entity/Picture'
import BackgroundImage from './BackgroundImage'
import Meta from './Meta'

import { PICTURES_NEXT_PAGE } from '../../constants/actionTypes'

export interface IProps {
  dispatch: Dispatch
  pictures: {
    current?: Picture
    nextPicture: string
    prevPicture: string
  }
}

export default class PictureViewer extends React.Component<IProps> {
  constructor(props) {
    super(props)

    this.handleClose = this.handleClose.bind(this)
    this.handleNext = this.handleNext.bind(this)
    this.handlePrev = this.handlePrev.bind(this)
  }

  public render() {
    return (
      <React.Fragment>
        <div className="picture-viewer">
          <KeyHandler keyEventName={KEYPRESS} keyValue="ArrowLeft" onKeyHandle={this.handlePrev} />
          <KeyHandler keyEventName={KEYPRESS} keyValue="k" onKeyHandle={this.handlePrev} />
          <button className="prev-btn" onClick={this.handlePrev}>&lt;</button>
          <BackgroundImage
            src={this.props.pictures.current.url}
            placeholder={this.props.pictures.current.placeholderUrl}
          />
          <button className="next-btn" onClick={this.handleNext}>&gt;</button>
          <KeyHandler keyEventName={KEYPRESS} keyValue="ArrowRight" onKeyHandle={this.handleNext} />
          <KeyHandler keyEventName={KEYPRESS} keyValue="j" onKeyHandle={this.handleNext} />
          <button className="close-btn close" onClick={this.handleClose}>
            <span aria-hidden="true">&times;</span>
          </button>
          <KeyHandler keyEventName={KEYPRESS} keyValue="Escape" onKeyHandle={this.handleClose} />
        </div>
        <Meta picture={this.props.pictures.current} />
      </React.Fragment>
    )
  }

  public handleClose() {
    this.props.dispatch(push('/'))
  }

  public handleNext() {
    if (this.props.pictures.nextPicture) {
      this.props.dispatch(push(`/picture/${this.props.pictures.nextPicture}`))
    } else {
      this.props.dispatch({type: PICTURES_NEXT_PAGE})
    }
  }

  public handlePrev() {
    this.props.dispatch(push(`/picture/${this.props.pictures.prevPicture}`))
  }
}
