import * as React from 'react'
import LazyImage from '../Image/LazyImage'

export interface IProps {
  src: string
  placeholder: string
}

// Based on https://github.com/danieltimpone/react-background-image-loader/blob/master/lib/BackgroundImage.js
export default class BackgroundImage extends React.Component<IProps> {
  public render() {
    return (
      <React.Fragment>
        <LazyImage
          src={this.props.src}
        >
          <div
            className="image-wrapper"
            style={{backgroundImage: `url(${this.props.src})`}}
          />
        </LazyImage>
      </React.Fragment>
    )
  }
}
