import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import * as React from 'react'
import Picture from '../../../domain/Entity/Picture'
import PictureViewer from './PictureViewer'

Enzyme.configure({ adapter: new Adapter() });

function setup(testProps = {}) {
  const props = {
    pictures: {
      current: new Picture({})
    }
  }

  const finalProps = {...props, ...testProps}

  const enzymeWrapper = Enzyme.shallow(
    <PictureViewer {...finalProps} />
  )

  return {
    enzymeWrapper,
    props,
  }
}

describe('components', () => {
  describe('PictureViewer', () => {
    it('should render Picture', () => {
      const picture = new Picture({url: 'http://testimg.jpg'})
      const { enzymeWrapper } = setup({picture})

      expect(enzymeWrapper.find('.picture-viewer').exists()).toBe(true)
    })
  })
})
