import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'
import * as React from 'react'
import Picture from '../../../domain/Entity/Picture'
import BackgroundImage from './BackgroundImage'

Enzyme.configure({ adapter: new Adapter() });

function setup(testProps = {}) {
  const props = {
    src: '',
  }

  const finalProps = {...props, ...testProps}

  const enzymeWrapper = Enzyme.shallow(
    <BackgroundImage {...finalProps} />
  )

  return {
    enzymeWrapper,
    props,
  }
}

describe('components', () => {
  describe('BackgroundImage', () => {
    it('should render Picture', () => {
      const picture = new Picture({url: 'http://testimg.jpg'})
      const { enzymeWrapper } = setup({picture})

      expect(enzymeWrapper.find('LazyImage').exists()).toBe(true)
    })
  })
})
