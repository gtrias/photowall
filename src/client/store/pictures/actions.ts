import { Action, ActionCreator } from 'redux'
import Picture from '../../../domain/Entity/Picture'
import { FETCH_PICTURES, FETCH_PICTURES_FULLFILLED } from '../../constants/actionTypes';
import { IPicturesReceivedAction } from './types'

export const picturesReceived: ActionCreator<IPicturesReceivedAction> = (pictures: Picture[]) => ({
  payload: {
    pictures
  },
  type: FETCH_PICTURES_FULLFILLED
})

export const fetchPictures: ActionCreator<Action> = () => ({
  type: FETCH_PICTURES
})
