import Picture from '../../../domain/Entity/Picture'
import {
  FETCH_PICTURES,
  FETCH_PICTURES_FULLFILLED,
  FETCH_PICTURES_REJECTED,
  PICTURES_NEXT_PAGE,
  SET_CURRENT_PICTURE
} from '../../constants/actionTypes'
import reducer, { initialState } from './reducer'

describe('picture reducer', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {type: 'whatever'})).toEqual(initialState)
  })

  const picture1 = new Picture({id: '1234'})
  const picture2 = new Picture({id: '123'})
  const picture3 = new Picture({id: '321'})
  const picturesArr = [picture1,picture2,picture3]
  const picturesObj = {
    '123': picture2,
    '1234': picture1,
    '321': picture3
  }

  it('should handle FETCH_PICTURES', () => {
    const expectedState = {
      current: null,
      error: null,
      keys: [],
      loading: true,
      nextPicture: null,
      page: '1',
      pictures: {},
      prevPicture: null,
    }
    expect(reducer(initialState, {type: FETCH_PICTURES})).toEqual(expectedState)
  })

  it('should save the error on FETCH_PICTURES_REJECTED', () => {
    const expectedState = {
      current: null,
      error: 'This is null',
      keys: [],
      loading: false,
      nextPicture: null,
      page: '1',
      pictures: {},
      prevPicture: null,
    }
    expect(reducer(initialState, {type: FETCH_PICTURES_REJECTED, message: 'This is null'})).toEqual(expectedState)
  })

  it('should handle PICTURES_NEXT_PAGE', () => {
    const expectedState = {
      current: null,
      error: null,
      keys: [],
      loading: true,
      nextPicture: null,
      page: '2',
      pictures: {},
      prevPicture: null,
    }
    expect(reducer(initialState, {type: PICTURES_NEXT_PAGE})).toEqual(expectedState)
  })

  it('should handle SET_CURRENT_PICTURE', () => {
    const expectedState = {
      current: picture1,
      error: null,
      keys: ['123', '1234', '321'],
      loading: false,
      nextPicture: '321',
      page: '1',
      pictures: picturesObj,
      prevPicture: '123',
    }
    expect(
      reducer(
        {
          error: null,
          keys: ['123', '1234', '321'],
          loading: false,
          nextPicture: null,
          page: '1',
          pictures: picturesObj,
          prevPicture: null,
      }, {type: SET_CURRENT_PICTURE, pictureId: '1234'})
    ).toEqual(expectedState)
  })

  it('should save the objects on FETCH_PICTURES_FULLFILLED', () => {
    const expectedState = {
      current: null,
      error: null,
      keys: ['1234', '123', '321'],
      loading: false,
      nextPicture: null,
      page: '1',
      pictures: picturesObj,
      prevPicture: null,
    }
    expect(reducer(initialState, {type: FETCH_PICTURES_FULLFILLED, pictures: {pictures: picturesArr}}))
      .toEqual(expectedState)
  })

})
