import { Action } from 'redux'
import Picture from '../../../domain/Entity/Picture'

export interface IPicturesState {
  pictures: object
  current?: Picture
  error: string
  page: string
  loading: boolean
  nextPicture: string
  prevPicture: string
  keys: any[]
}

export interface IPicturesReceivedAction extends Action {
  type: string,
  payload: any
}

export type PictureActions = IPicturesReceivedAction
