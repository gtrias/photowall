import { Reducer } from 'redux'
import Picture from '../../../domain/Entity/Picture'
import {
  FETCH_PICTURES,
  FETCH_PICTURES_FULLFILLED,
  FETCH_PICTURES_REJECTED,
  PICTURES_NEXT_PAGE,
  SET_CURRENT_PICTURE,
} from '../../constants/actionTypes'
import { IPicturesState, PictureActions } from './types'

export const initialState: IPicturesState = {
  current: null,
  error: null,
  keys: [],
  loading: false,
  nextPicture: null,
  page: '1',
  pictures: {},
  prevPicture: null,
}

// Pictures reducer
const reducer: Reducer<IPicturesState> = (state: IPicturesState = initialState, action: any) => {
  switch ((action as PictureActions).type) {
    case FETCH_PICTURES:
      return {...state, loading: true}

    case FETCH_PICTURES_FULLFILLED:
      const pictures = state.pictures
      const keys = []

      action.pictures.pictures.forEach((result: any) => {
        const picture = new Picture(result)
        pictures[picture.id] = picture
        keys.push(result.id)
      })

      return {...state, pictures, loading: false, keys: [...state.keys, ...keys]}

    case FETCH_PICTURES_REJECTED:
      return {...state, error: action.message, loading: false}

    case PICTURES_NEXT_PAGE:
      return {...state, page: String(+state.page + 1), loading: true}

    case SET_CURRENT_PICTURE:
      const nextIndex = state.keys.indexOf(action.pictureId) + 1
      const prevIndex = state.keys.indexOf(action.pictureId) - 1
      const nextPicture = state.keys[nextIndex] ? state.keys[nextIndex].toString() : null
      const prevPicture = state.keys[prevIndex] ? state.keys[prevIndex].toString() : null
      return {...state, current: state.pictures[action.pictureId], prevPicture, nextPicture}

    // Return default state
    default:
      return state
  }
}

export default reducer
