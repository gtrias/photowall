import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import { FETCH_PICTURES, FETCH_PICTURES_FULLFILLED } from '../../constants/actionTypes'
import * as actions from './actions'

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('actions', () => {
  describe('pictures', () => {
    it('should receive pictures', () => {
      const expectedActions = [{type: FETCH_PICTURES_FULLFILLED, payload: {pictures: []}}]

      const store = mockStore({})

      store.dispatch(
        actions.picturesReceived([])
      )
      expect(store.getActions()).toEqual(expectedActions)
    })

    it('should fetch pictures', () => {
      const expectedActions = [{type: FETCH_PICTURES}]

      const store = mockStore({})

      store.dispatch(
        actions.fetchPictures([])
      )
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
