import { combineReducers, Reducer } from 'redux'
import picturesReducer from './pictures/reducer'
import { IPicturesState } from './pictures/types'

// Global application state interface
export interface IApplicationState {
  pictures: IPicturesState
}

// All combined reducers
export const reducers: Reducer<IApplicationState> = combineReducers<IApplicationState>({
  pictures: picturesReducer
})
