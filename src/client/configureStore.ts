import { connectRouter, routerMiddleware } from 'connected-react-router'
import {
  applyMiddleware,
  compose,
  createStore,
  Store,
} from 'redux'
import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk'

import picturesSaga from './sagas'
import { IApplicationState, reducers } from './store'

// Configure app store
export default function configureStore(history): Store<IApplicationState> {
    // create the composing function for our middlewares
  const composeEnhancer: typeof compose = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  const initialState = {}
  const sagaMiddleware = createSagaMiddleware()

  // Reducers
  const combReducers = connectRouter(history)(reducers)

  // Prepare store with all the middlewares
  const store = createStore<any, any, any, any> (
    combReducers,
    initialState,
    composeEnhancer(
      applyMiddleware(thunk),
      applyMiddleware(sagaMiddleware),
      applyMiddleware(routerMiddleware(history)),
    ),
  )

  // Running sagas
  sagaMiddleware.run(picturesSaga)

  return store
}
