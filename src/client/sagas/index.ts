import { LOCATION_CHANGE } from 'connected-react-router'
import { call, put, select, takeEvery } from 'redux-saga/effects'

import ApiRepository from '../../domain/Repository/ApiRepository'
import GetPicturesRequest from '../../domain/Service/GetPicturesRequest'
import GetPicturesService from '../../domain/Service/GetPicturesService'
import {
  FETCH_PICTURES,
  FETCH_PICTURES_FULLFILLED,
  FETCH_PICTURES_REJECTED,
  PICTURES_NEXT_PAGE,
  SET_CURRENT_PICTURE,
  SET_CURRENT_PICTURE_REJECTED
} from "../constants/actionTypes"
import { IPicturesReceivedAction } from '../store/pictures/types'

const getPictures = state => state.pictures

// Fetch pictures saga
export function* fetchPictures(action: IPicturesReceivedAction) {
  const picturesState = yield select(getPictures)

  const picturesService = new GetPicturesService(new ApiRepository(process.env.REACT_APP_API_URL))
  const picturesRequest = new GetPicturesRequest({
    query: {
      page: picturesState.page
    }
  })
  try {
    const pictures = yield call([picturesService, 'execute'], picturesRequest)
    yield put({ type: FETCH_PICTURES_FULLFILLED, pictures: pictures.pictures})
  } catch (e) {
    yield put({ type: FETCH_PICTURES_REJECTED, message: e.message })
  }
}

// Set curren picture from routing
export function* setCurrentPicture(action) {
  if (!action.payload) {
    return
  }

  try {
    const pictureId = /\/picture\/(.*)/g.exec(action.payload.location.pathname)
    if (pictureId && pictureId[1] !== null) {
      yield put({
        pictureId: pictureId[1],
        type: SET_CURRENT_PICTURE,
      })
    } else {
      yield put({
        pictureId: null,
        type: SET_CURRENT_PICTURE,
      })
    }
  } catch (e) {
    yield put({
      type: SET_CURRENT_PICTURE_REJECTED
    })
  }
}

// SET_CURRENT_PICTURE after FETCH_PICTURES_FULLFILLED
export function* refillCurrent(action) {
  const picturesState = yield select(getPictures)

  if (picturesState.current) {
    // Upgly way to refill the nextPicture reducer
    yield put({ type: SET_CURRENT_PICTURE, pictureId: picturesState.current.id})
  }
}

function* picturesSaga() {
  yield takeEvery(FETCH_PICTURES, fetchPictures)
  yield takeEvery(FETCH_PICTURES_FULLFILLED, refillCurrent)
  yield takeEvery(PICTURES_NEXT_PAGE, fetchPictures)
  yield takeEvery(LOCATION_CHANGE, setCurrentPicture)
}

export default picturesSaga
