import { connect } from 'react-redux'
import Wall from '../components/Wall/Wall'
import { IApplicationState } from '../store'

export default connect(
  (state: IApplicationState, ownProps) => ({
    pictures: state.pictures,
    routing: ownProps
  })
)(Wall)
