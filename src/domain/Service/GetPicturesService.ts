import IRepository from '../Repository/IRepository'
import GetPicturesRequest from './GetPicturesRequest'
import GetPicturesResponse from './GetPicturesResponse'
import IService from './IService'

export default class GetPicturesService implements IService {
  public repository: IRepository

  public constructor(repository: IRepository) {
    this.repository = repository
  }

  public execute(request: GetPicturesRequest): Promise<any> {
    return new Promise((resolve, reject) => {
      this.repository
        .find(request)
        .then((pictures) => {
          const response = new GetPicturesResponse(pictures, request.getPage())
          return resolve(response)
        })
        .catch((err) => {
          return reject(err)
        })
    })
  }
}
