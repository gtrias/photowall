import { expect } from 'chai'

import DummyRepository from '../Repository/DummyRepository'
import GetPicturesRequest from './GetPicturesRequest'
import GetPicturesService from './GetPicturesService'

describe('Domain', () => {
  describe('Service', () => {
    describe('PictureService', () => {
      it('should return images promise', () => {
        const getPictureService = new GetPicturesService(new DummyRepository())
        expect(getPictureService.execute(new GetPicturesRequest({query: {page: '1'}}))).to.be.an('promise')
      })
    })
  })
})
