import IRepository from '../Repository/IRepository'
import IServiceResponse from './IServiceResponse'

export default interface IService {
  repository: IRepository
  execute(query: any): Promise<IServiceResponse>
}
