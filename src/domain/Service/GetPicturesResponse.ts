import Picture from '../Entity/Picture'

export default class GetPicturesResponse {
  public pictures: Picture[]
  public page: string

  constructor(pictures: [], page: string) {
    this.pictures = pictures
    this.page = page || '1'
  }
}
