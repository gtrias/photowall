interface IRequest {
  query: {
    page: string
  }
}

export default class GetPicturesRequest {
  public page: string

  constructor(req: IRequest) {
    this.page = req.query.page || '1'
  }

  public getPage() {
    return this.page
  }
}
