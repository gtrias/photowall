import Picture from '../Entity/Picture'

export default class PicturesDTO {
  private pictures: Picture[] = []

  public constructor(data?: any[]) {
    if (data) {
      data.forEach((elem) => {
        this.pictures.push(new Picture(elem))
      })
    }
  }

  public fillFromFlickr(data: any[]) {
    if (data) {
      data.forEach((elem) => {
        const url = `https://farm${elem.farm}.staticflickr.com/${elem.server}/${elem.id}_${elem.secret}.jpg`
        const author = {
          userId: elem.owner,
          username: elem.ownername,
        }
        this.pictures.push(new Picture({
          ...elem,
          author,
          description: elem.description._content,
          height: elem.height_o,
          placeholderUrl: elem.url_t,
          postUrl: `https://www.flickr.com/photos/${elem.owner}/${elem.id}`,
          tags: elem.tags.split(' '),
          thumbUrl: elem.url_n,
          url: elem.url_k || url,
          width: elem.width_o,
        }))
      })
    }
  }

  public getPictures() {
    return this.pictures
  }
}
