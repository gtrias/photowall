import { expect } from 'chai'

import PicturesDTO from './PicturesDTO'

describe('Domain', () => {
  describe('DTO', () => {
    describe('PicturesDTO', () => {
      it('should convert plain array to Pictures array', () => {
        const pictures = [
          {id: 1234},
          {id: 1234},
          {id: 1234},
        ]
        const dto = new PicturesDTO(pictures)
        expect(dto.getPictures()).to.be.a('array')
      })
    })
  })
})
