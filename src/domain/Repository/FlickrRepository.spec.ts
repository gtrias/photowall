import { expect } from 'chai'

import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import GetPicturesRequest from '../Service/GetPicturesRequest'
import FlickrRepository from './FlickrRepository'

const mock = new MockAdapter(axios);

// Mock any GET request to /users
// arguments for reply are (status, data, headers)
mock.onAny(/.*/).reply(200, {
  photos: [
    { id: 1, url: 'myphoto' }
  ]
});

describe('Domain', () => {
  describe('Repository', () => {
    describe('FlickrRepository', () => {
      it('should return images', () => {
        const pictureRepo = new FlickrRepository('12345', axios)
        const getPicReq = new GetPicturesRequest({
          query: {
            page: '1'
          }
        })
        expect(pictureRepo.find(getPicReq)).to.be.a('promise')
      })
    })
  })
})
