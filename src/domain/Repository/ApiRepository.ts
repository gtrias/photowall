import axios from 'axios'
import PicturesDTO from '../DTO/PicturesDTO'
import GetPicturesRequest from '../Service/GetPicturesRequest'
import IRepository from './IRepository'

export default class ApiRepository implements IRepository {
  public baseUrl

  public constructor(baseUrl) {
    this.baseUrl = baseUrl || process.env.API_URL || 'http://localhost:4000'
  }

  public find(request: GetPicturesRequest): Promise<any> {
    return new Promise((resolve, reject) => {
      const requestParams = {
        page: request.getPage() || 1,
      }

      const queryStr = Object.keys(requestParams)
        .map(key => key + '=' + requestParams[key])
        .join('&')

      return axios.get(this.baseUrl + '/pictures' + '?' + queryStr)
      .then((result) => {
        resolve(new PicturesDTO(result.data.pictures))
      })
      .catch((err) => {
        reject(err)
      })
    })
  }
}
