import Picture from '../Entity/Picture'
import IRepository from './IRepository'

export default class DummyRepository implements IRepository {
  public find() {
    const pictures = []

    for (let i = 0; i < 50; i++) {
      const width = i % 3 === 0 ? 200: 300;
      const height = i % 3 === 0 ? 300: 200;
      const placeholderUrl = this.getPlaceholderUrl(width, height)
      pictures.push(
        new Picture({id: i, url: placeholderUrl + i})
      )
    }
    return Promise.resolve(pictures)
  }

  private getPlaceholderUrl(width: number = 500, height: number = 200): string {
    const placeholderUrl = `https://picsum.photos/${width}/${height}/?random`
    return placeholderUrl
  }
}
