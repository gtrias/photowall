import axios from 'axios'
import PicturesDTO from '../DTO/PicturesDTO'
import IRepository from './IRepository'

export default class FlickrRepository implements IRepository {
  public baseUrl = 'https://api.flickr.com/services/rest'
  private apikey: string
  private client: any

  constructor(apikey: string, client = axios) {
    this.apikey = apikey
    this.client = client
  }

  public find(query: any): Promise<any> {

    const requestParams = {
      api_key: this.apikey,
      extras: 'owner_name, tags, description, url_n, url_t, url_o, url_k',
      format: 'json',
      method: 'flickr.photos.search',
      nojsoncallback: 1,
      page: query.getPage(),
      per_page: 40,
      sort: 'relevance',
      tags: 'wallpaper, fantasy',
      text: 'wallpaper',
    }

    const queryStr = Object.keys(requestParams)
      .map(key => key + '=' + requestParams[key])
      .join('&')

    return new Promise((resolve, reject) => {
      const finalUrl = this.baseUrl + '?' + queryStr
      return this.client.get(finalUrl)
        .then((result) => {
          if (!result.data.photos) {
            throw new Error('API request has failed')
          }

          // Fill DTO from flickr format
          const picturesDto = new PicturesDTO()
          picturesDto.fillFromFlickr(result.data.photos.photo)

          // Return DTO to service
          resolve(picturesDto)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }
}
