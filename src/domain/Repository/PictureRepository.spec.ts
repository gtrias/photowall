import { expect } from 'chai'

import PictureRepository from './PictureRepository'

describe('Domain', () => {
  describe('Repository', () => {
    describe('PictureRepository', () => {
      it('should return images', () => {
        const pictureRepo = new PictureRepository()
        expect(pictureRepo.find()).to.be.a('promise')
      })
    })
  })
})
