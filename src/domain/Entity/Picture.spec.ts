import { expect } from 'chai'

import Picture from './Picture'

describe('Domain', () => {
  describe('Entity', () => {
    describe('Picture', () => {
      it('should return id', () => {
        const picture = new Picture({url: 'http://test-img.jpg'})
        expect(picture.id).to.be.a('string')
      })

      it('should return url', () => {
        const picture = new Picture({url: 'http://test-img.jpg'})
        expect(picture.url).to.be.a('string')
      })

      it('shoudl calc proportional height from width', () => {
        const picture = new Picture({
          height: 400,
          url: 'http://test-img.jpg',
          width: 300,
        })
        expect(picture.getProportionalHeight(150)).to.be.a('number')
        expect(picture.getProportionalHeight(150)).to.equal(200)
      })
    })
  })
})
