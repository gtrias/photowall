import { v4 as uuid } from 'uuid'

interface IAuthor {
  userId: string
  username: string
}

export default class Picture {
  public id: string
  public title: string
  public description: string
  public url: string
  public placeholderUrl: string
  public thumbUrl: string
  public author: IAuthor
  public tags: string[]
  public width: number
  public height: number
  public postUrl: string

  constructor(pictureData: any) {
    this.id = pictureData.id || uuid()
    this.url = pictureData.url
    this.placeholderUrl = pictureData.placeholderUrl || pictureData.url
    this.thumbUrl  = pictureData.thumbUrl || pictureData.url
    this.title  = pictureData.title
    this.description = pictureData.description
    this.author = pictureData.author || {}
    this.tags = pictureData.tags || []
    this.width = pictureData.width
    this.height = pictureData.height
    this.postUrl = pictureData.postUrl
  }

  // returns the proportional height based on
  // width and original offset
  public getProportionalHeight(width: number) {
    return this.height * width / this.width
  }
}
