import cors = require('cors')
import express = require('express')
import { PicturesController } from './controllers/'

const app: express.Application = express()

app.use(express.static('build'))
app.use(cors())

app.use('/pictures', PicturesController)

export default app
