import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import * as chai from 'chai'
import * as supertest from 'supertest'
import app from '../app'

const expect = chai.expect

const mock = new MockAdapter(axios);

// Mock any GET request to /users
// arguments for reply are (status, data, headers)
mock.onAny(/.*/).reply(200, {
  photos: [
    { id: 1, url: 'myphoto' }
  ]
});


describe('API', () => {
  let request = null
  let server = null

  beforeAll((done) => {
    server = app.listen(done)
    request = supertest.agent(server)
  })

  afterAll((done) => {
    server.close(done)
  })

  describe('Controller', () => {
    describe('PicturesController', () => {
      it('should return images', () => {
        request.get('/pictures')
          .end((err, res) => {
            expect(err).to.be.equal(null)
            expect(res.status).to.be.equal(200)
            expect(res.header['x-page']).to.equal('1')
          })
      })

      it('should handle pagination', (done) => {
        request.get('/pictures?page=2')
          .end((err, res) => {
            expect(err).to.be.equal(null)
            expect(res.status).to.be.equal(200)
            expect(res.header['x-page']).to.equal('2')
            done()
          })
      })
    })
  })
})
