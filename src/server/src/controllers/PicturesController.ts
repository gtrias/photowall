// Source https://github.com/BrianDGLS/express-ts
// Import only what we need from express
import { Request, Response, Router } from 'express'
import FlickrRepository from '../../../domain/Repository/FlickrRepository'
import GetPicturesRequest from '../../../domain/Service/GetPicturesRequest'
import GetPicturesService from '../../../domain/Service/GetPicturesService'

// Assign router to the express.Router() instance
const router: Router = Router()

// The / here corresponds to the route that the WelcomeController
// is mounted on in the server.ts file.
// In this case it's /pictures
router.get('/', (req: Request, res: Response) => {

  // Prepare GetPicturesService with FlickrRepository
  const pictureService = new GetPicturesService(
    new FlickrRepository(process.env.FLICKR_APIKEY)
  )

  // Map the request to domain GetPicturesRequest
  const request = new GetPicturesRequest(req)

  // Execute the query and return the response
  pictureService.execute(request).then((picturesResponse) => {
    res.set('X-Page', picturesResponse.page)
    res.send(picturesResponse.pictures)
  })
    .catch((err) => {
      res.status(500).send(err.message)
    })
})

export const PicturesController: Router = router
