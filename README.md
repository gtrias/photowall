# Photowall

Just a photo wall gallery made with <3 and reactjs

* [Online demo](https://genar-photowall.herokuapp.com)

## Features

* The webpack and babel configuration is coming from react-create-app however the app was ejected (`npm run eject`) in order to fine-tunning the configurations.
* Coded using [typescript](https://www.typescriptlang.org/)
* Shared domain code between backend and frontend. Trying to apply some DDD concepts since I just readed [this book](https://leanpub.com/ddd-in-php) at holidays.
* TDD project.
* CI / CD enabled
* For lazy image loading I've used the library [react-lazy-load-image-component](https://www.npmjs.com/package/react-lazy-load-image-component) to don't reinvent the wheel.
* It uses [redux-saga](https://github.com/redux-saga/redux-saga) to manage side effect model.
* For the wall layout I've used [masonry library](https://github.com/paulcollett/react-masonry-css) for its good cross browser compatibility and because at the moment there [no masonry layout doable with pure css](https://regisphilibert.com/blog/2017/12/pure-css-masonry-layout-with-flexbox-grid-columns-in-2018/).

## Notes

* This is my second development using typescript (the first using reactjs) so beware :dragon:
* The online demo is hosted in [heroku](https://heroku.com) so it may response slowly the first load because [Dyno sleeping](https://devcenter.heroku.com/articles/dynos#dyno-sleeping)

## Install

Just clone the repo and run `yarn` or `npm i`

```bash
git clone git@gitlab.com:gtrias/photowall.git
```

```bash
npm i
```

## Run

```bash
npm start
```

## Run development environment

```bash
npm run dev
```

## Test

```bash
npm test
```

## Setting up flickr credentials

Expose the environment variable `FLICKR_APIKEY` with your apikey

```bash
FLICKR_APIKEY=my-api-key npm start
```

## Setting API endpoint

Expose the env var `API_URL` to set the api url.


## Running with docker

```
docker run -d -p 4000:4000 -e FLICKR_APIKEY=your-api-key -e API_URL='http://localhost:4000' registry.gitlab.com/gtrias/photowall
```
