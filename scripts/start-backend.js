const { choosePort } = require('react-dev-utils/WebpackDevServerUtils');

const restApiApp = require('../build/server/src/app')

const port = process.env.PORT || 4000
const HOST = process.env.HOST || '0.0.0.0';

// Running API Rest server
const server = restApiApp.default.listen(port, '0.0.0.0', () => {
  console.log('photowall REST API is listening to port %s', port)
})
